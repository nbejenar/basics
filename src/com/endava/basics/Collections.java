package com.endava.basics;

import java.util.ArrayList;
import java.util.List;

import static com.endava.basics.Collections.*;

public class Collections {
    static Result result;
    static final Object lock = new Object();

    public static void main(String[] args) {
        Thread producerThread = new Thread(new Producer());
        Thread supplierThread = new Thread(new Supplier());

        producerThread.start();
        supplierThread.start();
    }
}

class Result {
    public int value;

    public Result(int value){
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

class Producer implements Runnable{

    @Override
    public void run() {
        synchronized (lock){
            result = new Result(2);
            lock.notifyAll();
        }
    }
}

class Supplier implements Runnable {

    @Override
    public void run() {
        synchronized (lock){
            while(result == null){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println(result.value);
        }
    }
}

