package com.endava.basics;

public class ImmutableDemo {
    public static void main(String[] args) {
        Immutable1 o = new Immutable1(new Object());
        System.out.println(o.getValue());
        Object number = o.getValue();
        number = new Object();
        System.out.println(o.getValue());
        System.out.println(number);
    }
}

final class Immutable1{
    final Object value;

    public Immutable1(Object value){
        this.value = value;
    }

    public Immutable1(Immutable1 copy){
        this.value = copy.value;
    }

    public Object getValue() {
        return value;
    }
}
